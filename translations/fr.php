<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{productextrafields}prestashop>productextrafields_9f5a2cdfb571fde83f512ff0035ba5e6'] = 'I-tou - Nouveaux champs Produit';
$_MODULE['<{productextrafields}prestashop>productextrafields_3bc4bfc4dfe250c0413dda519298feb6'] = 'Affiche et ajoute des champs Produit';
$_MODULE['<{productextrafields}prestashop>hookdisplayadminproductsextra_1e62e6ce22608229f9e7d30baae981ee'] = 'Vendeur';
$_MODULE['<{productextrafields}prestashop>hookdisplayadminproductsextra_28626e52483b09917777f6c184c14518'] = 'Sélectionner un vendeur';
$_MODULE['<{productextrafields}prestashop>hookdisplayadminproductsextra_65fba9ec06765f60ab9510fff2bd2b16'] = 'Acheteur';
$_MODULE['<{productextrafields}prestashop>hookdisplayadminproductsextra_edaeb492ff59be178804918a34071ae3'] = 'Sélectionner un acheteur';
$_MODULE['<{productextrafields}prestashop>hookdisplayadminproductsextra_d79cf3f429596f77db95c65074663a54'] = 'ID commande';
