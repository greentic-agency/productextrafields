<?php

namespace Modules\Itou\ProductExtraFields;

use Modules\Itou\ProductExtraFields\ProductExtraFieldsModel as ProductExtraFieldsModel;
use Modules\Itou\ProductExtraFields\Module\Installable;
use Modules\Itou\ProductExtraFields\Module\Utils;


use Module as PrestashopModule;
use Tools;
use Context;
use DbQuery;
use Db;
use FormField;
use Customer;


class Module extends PrestashopModule
{
    const _TABLE_MODULE_ = 'productextrafields';
   
    use Installable;
    use Utils;

    protected $hooks = [
        'displayAdminProductsExtra',
        'actionAdminProductsFormModifier',
        'additionalProductFormFields',
        'actionObjectProductAddAfter',
        'actionObjectProductUpdateAfter',
        'actionObjectProductUpdateBefore',
        'actionObjectProductDeleteAfter',
        'actionAdminProductsListingFieldsModifier',
        'displayAdminCatalogTwigProductFilter',
        'displayAdminCatalogTwigProductHeader'
    ];

    /**
     * Custom Hook
     */
    public function hookDisplayAdminCatalogTwigProductHeader($params)
    {
        return $this->display(_PS_MODULE_DIR_.'productextrafields/productextrafields.php', 'src/views/templates/hook/displayAdminCatalogTwigProductHeader.tpl'); 
    }

    /**
     * Custom Hook
     */
    public function hookDisplayAdminCatalogTwigProductFilter($params)
    {
        $this->context->smarty->assign(
            [
                'filter_column_seller_lastname' => Tools::getValue('filter_column_seller_lastname',''),
                'filter_column_buyer_lastname' => Tools::getValue('filter_column_buyer_lastname',''),
            ]
        );

        return $this->display(_PS_MODULE_DIR_.'productextrafields/productextrafields.php', 'src/views/templates/hook/displayAdminCatalogTwigProductFilter.tpl'); 
    }

    public function hookActionAdminProductsListingFieldsModifier($list)
    {

        if (isset($list['sql_select'])) {
            $list['sql_select']['seller_firstname'] = array(
                "table"=>"v",
                "field"=>"firstname"
            );
            $list['sql_select']['seller_lastname'] = array(
                "table"=>"v",
                "field"=>"lastname",
                'filtering' => 'LIKE \'%%%s%%\''
            );
            $list['sql_select']['buyer_firstname'] = array(
                "table"=>"u",
                "field"=>"firstname"
            );
            $list['sql_select']['buyer_lastname'] = array(
                "table"=>"u",
                "field"=>"lastname",
                'filtering' => 'LIKE \'%%%s%%\''
            );
        }

        if (isset($list['sql_table'])) {
            $list['sql_table']['pef'] = array(
                "table"=> self::_TABLE_MODULE_,
                "join"=>"LEFT JOIN",
                "on"=>"pef.`id` = p.`id_product`"
            );
            $list['sql_table']['v'] = array(
                "table"=> 'customer',
                "join"=>"LEFT JOIN",
                "on"=>"pef.`seller_id` = v.`id_customer`"
            );
            $list['sql_table']['u'] = array(
                "table"=> 'customer',
                "join"=>"LEFT JOIN",
                "on"=>"pef.`buyer_id` = u.`id_customer`"
            );
        }

        $seller_lastname_filter = Tools::getValue('filter_column_seller_lastname',false);
        if ( $seller_lastname_filter && $seller_lastname_filter != '') {
            $list['sql_where'][] .= "v.`lastname` LIKE '%".$seller_lastname_filter."%'";
        }
       
        $buyer_lastname_filter = Tools::getValue('filter_column_buyer_lastname',false);
        if ( $buyer_lastname_filter && $buyer_lastname_filter != '') {
            $list['sql_where'][] .= "u.`lastname` LIKE '%".$buyer_lastname_filter."%'";
        }
    }

    /**
     * Hook update datas to database - Front Office + Back Office
     * @param type $params
     */
    public function hookActionObjectProductUpdateAfter(array $params)
    {
        ProductExtraFieldsModel::saveNewFieldsRow($params['object']->id);
    }

    /**
     * Hook add datas to database from Front Office + Back Office
     * @param type $params
     */
    public function hookActionObjectProductAddAfter(array $params) 
    {
        ProductExtraFieldsModel::saveNewFieldsRow($params['object']->id);
    }

    /**
     * Delete datas from module table when product is deleted
     * @param type $params
     */
     public function hookActionObjectProductDeleteAfter(array $params)
     {  
        $productExtraFields = new ProductExtraFieldsModel($params['object']->id);
        return $productExtraFields->delete();
     }

    /**
     * Add the new fields on account product AUTO - Back Office
     * @param type $params
     * @return array
     */
    public function HookDisplayAdminProductsExtra(array $params) 
    {    
        $productExtraFieldsClass = new ProductExtraFieldsModel($params['id_product']);
    
        $this->context->smarty->assign(
            [
            'sellerId' => $productExtraFieldsClass->seller_id,
            'buyerId' => $productExtraFieldsClass->buyer_id,
            'isShippingCostPaid' => $productExtraFieldsClass->is_shipping_cost_paid,
            'isCancelled' => $productExtraFieldsClass->is_cancelled,
            'customersList' => Customer::getCustomers($onlyActive = true),
            'orderId' => $productExtraFieldsClass->order_id,
            'dateSend' => $productExtraFieldsClass->date_send,
            'dateCancel' => $productExtraFieldsClass->date_cancel,
            'dateRelaunch' => $productExtraFieldsClass->date_relaunch,
            'sellerRelaisId' => $productExtraFieldsClass->seller_relais_id,
            'relaisColisInfos'=> $this->findRelaisInfos($productExtraFieldsClass->seller_relais_id)
            ]
        );

        return $this->display(_PS_MODULE_DIR_.'productextrafields/productextrafields.php', 'src/views/templates/hook/hookDisplayAdminProductsExtra.tpl'); 
    }

    /**
     * 
     */
    public function findRelaisInfos($seller_relais_id)
    {
        if(Module::isInstalled('relaiscolis') && $seller_relais_id != '') {
            $query = (new DbQuery())
                    ->select('*')
                    ->from('relaiscolis_info', 'r')
                    ->where('r.id_relais_colis_info = ' . pSQL($seller_relais_id));

        if(Db::getInstance()->getRow($query))
            return Db::getInstance()->getRow($query);
        else
            return [];
        }

        return [];
    }
}