 <th>
    <input
        type="text"
        class="form-control"
        placeholder="{l s='Chercher vendeur (nom de famille)' mod='productextrafields'}"
        name="filter_column_seller_lastname"
        value="{$filter_column_seller_lastname}"
    />
</th>
<th class="text-center">
    <input
        type="text"
        class="form-control"
        placeholder="{l s='Chercher acheteur (nom de famille)' mod='productextrafields'}"
        name="filter_column_buyer_lastname"
        value="{$filter_column_buyer_lastname}"
    />
 </th>