<input type='hidden' name='date_send' value='{$dateSend}'>
<input type='hidden' name='date_cancel' value='{$dateCancel}'>
<input type='hidden' name='date_relaunch' value='{$dateRelaunch}'>

<div class='form-group'>
    <label class='form-control-label'>{l s='Seller' mod='productextrafields'}</label>
    <select name='seller_id' id='seller_id' class='form-control form-control-select'>
        <option value=''>{l s='Select a seller' mod='productextrafields'}</option>
        {foreach from=$customersList item='customer'}
            <option value='{$customer.id_customer}' {if $customer.id_customer == $sellerId }selected{/if}>
                {$customer.firstname} {$customer.lastname}
            </option>
        {/foreach}
    </select>
</div>
<div class='form-group'>
    <label class='form-control-label'>{l s='Buyer' mod='productextrafields'}</label>
    <select name='buyer_id' id='buyer_id' class='form-control form-control-select'>
    <option value=''>{l s='Select a buyer' mod='productextrafields'}</option>
        {foreach from=$customersList item='customer'}
            <option value='{$customer.id_customer}' {if $customer.id_customer == $buyerId}selected{/if}>
                {$customer.firstname} {$customer.lastname}
            </option>
        {/foreach}
    </select>
</div>
<div class='form-group'> 
    <label class='form-control-label'>{l s='Order ID' mod='productextrafields'}</label><br/>
    <input type="text" name='order_id' value='{$orderId}'>
</div>

<div class='form-group'> 
    <label>{l s='Le vendeur prend en charge les frais de port' mod='Modules.Itou'}</label>
     <br/>
    <label>{l s='Non'}</label>
    <input type="radio" id='no' name='is_shipping_cost_paid' value='0'  {if $isShippingCostPaid == false}checked{/if}>
    <label>{l s='Oui'}</label>
    <input type="radio" id='yes' name='is_shipping_cost_paid' value='1' {if $isShippingCostPaid == true}checked{/if}>
</div>

<div class='form-group'> 
    <label>{l s='Annulé' mod='Modules.Itou'}</label>
    <br/>
    <label>{l s='Non'}</label>
    <input type="radio" name='is_cancelled' value='0'  {if $isCancelled == false}checked{/if}>
    <label>{l s='Oui'}</label>
    <input type="radio" name='is_cancelled' value='1' {if $isCancelled == true}checked{/if}>
    {if $isCancelled == true}{if $dateCancel != ''} Le {$dateCancel|date_format}{/if}{/if}
</div>


    <div class='form-group'> 
        <label>{l s='Relance si l\'acheteur n\'a pas encore payé' mod='Modules.Itou'}</label><br/>
        {if $dateRelaunch != '' && $dateRelaunch != '0000-00-00 00:00:00'}
            Le {$dateRelaunch|date_format}
        {else}
            Pas de relance.
        {/if}
    </div>



<div class='form-group'> 
    <label class='form-control-label'>{l s='Point relais de dépôt colis' mod='productextrafields'}</label>
     <input type="hidden" name='seller_relais_id' value='{$sellerRelaisId}'>
    {if !empty($relaisColisInfos)}
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{$relaisColisInfos.rel_name}</h5>
                {$relaisColisInfos.rel_adr}<br/>
                {$relaisColisInfos.rel_cp}<br/>
                {$relaisColisInfos.rel_vil}
            </div>
        </div>
    {else}
        <br/>AUCUN pour le moment.
    {/if}
</div>



