<?php

namespace Modules\Itou\ProductExtraFields;
use Modules\Itou\Itou\Module\Service\Sms;
use Modules\Itou\Itou\Module\Service\Email;

use ObjectModel;
use Db;
use DbQuery;
use Tools;
use Product;
use Carrier;
use Hook;

class ProductExtraFieldsModel extends ObjectModel
{
    const _TABLE_MODULE_ = 'productextrafields';
    const _FREE_CARRIER_IDS_ = [1];
    const _FREE_CARRIER_ID_ = 1;

    /** @var string Seller ID */
    public $seller_id;

    /** @var string Buyer ID */
    public $buyer_id;

    /** @var bool  is_shipping_cost_paid */
    public $is_shipping_cost_paid;

    /** @var string sending date */
    public $date_send;

    /** @var string Order ID */
    public $order_id;

    /** @var string Seller relais ID */
    public $seller_relais_id;

    /** @var string cancelling date */
    public $date_cancel;

    /** @var string relaunch date */
    public $date_relaunch;

    /** @var bool  is_cancelled */
    public $is_cancelled;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'productextrafields',
        'primary' => 'id',
        'multilang' => false,
        'fields' => array(
            'seller_id' => array('type' => self::TYPE_INT, 'size' => 11, 'validate' => 'isNullOrUnsignedId', 'type_database' => 'INT', 'subscription_form' => false),
            'buyer_id' => array('type' => self::TYPE_INT, 'size' => 11, 'validate' => 'isNullOrUnsignedId', 'type_database' => 'INT', 'subscription_form' => false, 'allow_null' => true),
            'is_shipping_cost_paid' => array('type' => self::TYPE_BOOL, 'size' => 1, 'validate' => 'isBool', 'type_database' => 'TINYINT', 'subscription_form' => false),
            'date_send' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'subscription_form' => false, 'type_database' => 'DATETIME', 'allow_null' => true),
            'order_id' => array('type' => self::TYPE_INT, 'size' => 11, 'validate' => 'isNullOrUnsignedId', 'type_database' => 'INT', 'subscription_form' => false),
            'seller_relais_id' => array('type' => self::TYPE_INT, 'size' => 11, 'validate' => 'isNullOrUnsignedId', 'type_database' => 'INT', 'subscription_form' => false),
            'date_cancel' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'subscription_form' => false, 'type_database' => 'DATETIME', 'allow_null' => true),
            'date_relaunch' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'subscription_form' => false, 'type_database' => 'DATETIME', 'allow_null' => true),
            'is_cancelled' => array('type' => self::TYPE_BOOL, 'size' => 1, 'validate' => 'isBool', 'type_database' => 'TINYINT', 'subscription_form' => false)
        ),
    );

      /**
     * Builds the object
     *
     * @param int|null $id If specified, loads and existing object from DB (optional).
     * @param int|null $id_lang Required if object is multilingual (optional).
     * @param int|null $id_shop ID shop for objects with multishop tables.
     * @param PrestaShopBundle\Translation\Translator
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function __construct($id = null, $idLang = null)
    {
        parent::__construct($id, $idLang);
        $this->force_id = true;
    }

  
    /**
     * Saves current object to database (add or update)
     *
     * @param bool $null_values
     * @param bool $auto_date
     *
     * @return bool Insertion result
     * @throws PrestaShopException
     */
    public function save($null_values = true, $auto_date = true)
    {
        if(!ProductExtraFieldsModel::existsInDatabase($this->id, self::_TABLE_MODULE_)) {
            return $this->add($auto_date, $null_values);
        }else{
            return $this->update($null_values);
        }
    }

    /**
     * Checks if an object exists in database.
     *
     * @param int    $id_entity
     * @param string $table
     *
     * @return bool
     */
    public static function existsInDatabase($id_entity, $table)
    {
        $row = Db::getInstance()->getRow('
			SELECT `id` as id
			FROM `'._DB_PREFIX_.bqSQL($table).'` e
			WHERE e.`id` = '.(int)$id_entity, false
        );
        return isset($row['id']);
    }

    /**
     * Add or update datas to database
     * @param int $id_object
     * @return bool
     */
    public static function saveNewFieldsRow($id_object)
    {
       $datas = Tools::getAllValues();

       if(array_key_exists('buyer_id', $datas)) {
            $productExtraFields = new ProductExtraFieldsModel();
            $productExtraFields->id = $id_object;

            foreach($productExtraFields->getFields() as $key => $value) {
                if($key != "id"){
                    if(array_key_exists($key, $datas)) {
                        $productExtraFields->$key = $datas[$key];
                    } else {
                        $productExtraFields->$key = false;
                    }
                }
            }
            
            if(is_null($productExtraFields->date_send) 
                || $productExtraFields->date_send == '0000-00-00 00:00:00') {
                $productExtraFields->date_send = null;
            }

            if(is_null($productExtraFields->date_relaunch) 
                || $productExtraFields->date_relaunch == '0000-00-00 00:00:00' 
                || $productExtraFields->date_relaunch == false) {
                $productExtraFields->date_relaunch = null;
            }
          
            if($productExtraFields->is_cancelled == true 
                && (is_null($productExtraFields->date_cancel) 
                || $productExtraFields->date_cancel == '0000-00-00 00:00:00' 
                || empty($productExtraFields->date_cancel))) {

                    $productExtraFields->date_cancel = date("Y-m-d H:i:s");
                    $productExtraFields->sendNotificationCancelling($productExtraFields);

                    /** disable product */
                   Db::getInstance()->execute(
                       "UPDATE `ps_product_shop` SET `active` = '0'
                        WHERE `id_product` = ".pSQL($id_object).";");

            }elseif($productExtraFields->is_cancelled == false) {
                $productExtraFields->date_cancel = null;
            }

            // if($productExtraFields->is_shipping_cost_paid == true) {
            //     $productExtraFields->setFreeCarriers($id_object, self::_FREE_CARRIER_IDS_);
            // }else{
            //     $productExtraFields->setFreeCarriers($id_object);
            // }

            return $productExtraFields->save(true, true);
        }
    }

    /**
     * Send mail and SMS to buyer and seller
     * if cancelling
     * @param object productExtraFields
     */
    public function sendNotificationCancelling($productExtraFields)
    {
        Hook::exec('productExtraFieldsModelSendNotificationCancelling', [
            'productExtraFields' => &$productExtraFields
        ]);
    }

    /**
     * Get values from database
     * @param int $idCustomer 
     * @return array
     */
    public function getValues(int $idCustomer)
    {
        $query = (new DbQuery())
        ->select('*')
        ->from(self::_TABLE_MODULE_, 'c')
        ->where('c.id = ' . pSQL($idCustomer));

        if(Db::getInstance()->getRow($query))
            return Db::getInstance()->getRow($query);
        else
            return [];
    }

    /**
     * Set free carrier
     * @param array $free_carriers of ids carrier
     * @param id_product
     */
    public function setFreeCarriers($id_product, array $free_carriers = [])
    {
        if(empty($free_carriers)){
            $all_carriers =  $carrier_list = Carrier::getCarriers(1, true, false, false, null, Carrier::ALL_CARRIERS);
      
            foreach($all_carriers as $carrier){
                if($carrier['id_reference'] != self::_FREE_CARRIER_ID_)
                    $free_carriers[] = $carrier['id_reference'];
            }
        }
          
        $_POST['selectedCarriers'] = $free_carriers;

        $product = new Product($id_product);
        $product->setCarriers($free_carriers);
    
    }

}