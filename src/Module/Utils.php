<?php

namespace Modules\Itou\ProductExtraFields\Module;

trait Utils
{
     /**
     * @params int $idType
     * @return string
     */
    public function formateFieldType(int $idType, string $name)
    {
        $type = array();
        switch ($idType) {
            case 2 : 
                $type['type'] = 'switch';
                $type['type_form_front'] = 'checkbox';
                $type['is_bool'] = true;
                $type['value_form_front'] = true;
                $type['value'] = array(
                    array(
                        'id' => $name.'_on',
                        'value' => 1,
                        'label' => $this->trans('Enabled', array(), 'Admin.Global')
                    ),
                    array(
                        'id' => $name.'_off',
                        'value' => 0,
                        'label' => $this->trans('Disabled', array(), 'Admin.Global')
                    )
                );
                break;
            default :
                $type['type'] = 'text';
                $type['type_form_front'] = 'text';
                $type['value_form_front'] = '';
                $type['is_bool'] = false;
                $type['value'] = '';
                
                break;
        }

        return $type;
    }

    /**
     * Format fields to display value properly
     * @param string $FieldName to formate
     * @return string
     */
    public function formateFieldName(string $fieldName)
    {
        return  ucfirst(str_replace("_", " ", $fieldName));
    }

     /**
     * Fill current object with new fields
     * @param array $newFieldsValue
     * @param array $params
     * @return object
     */
    public function populateObject(array $newFieldsValue, $params)
    {
        foreach($newFieldsValue as $key => $newFieldValue){
            $params['object']->$key = $newFieldValue;
        }
    }

    /**
     * Remove taxes from Price
     * @param $price_without_taxes
     */
    public function calculatePriceWT($price_without_taxes, $taxe_percentage)
    {
        $price_with_taxes = $price_with_taxes / (1 + ($taxe_percentage/100));
        return $price_with_taxes;
    }
}