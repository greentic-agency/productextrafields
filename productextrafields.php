<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once __DIR__ . '/autoload.php';

class ProductExtraFields extends Modules\Itou\ProductExtraFields\Module
{
    /**
     * Module construct
     */
    public function __construct()
    {
        $this->name = 'productextrafields';
        $this->author = 'Lucile Greentic';
        $this->version = '1.0';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('I-tou - Product Extra Fields');
        $this->description = $this->l('Displays and adds product extra fields.');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
    }
}